const http = require('http');
const fs = require('fs');
const url = require('url');
const log = require('./utils/log')

module.exports = () => {

  const server = http.createServer(function (request, response) {
    const parsedUrl = url.parse(request.url, true);
    console.log(`Requested path: '${parsedUrl.pathname}'`);

    const pathname = parsedUrl.pathname;
    if (pathname === '/') {
      handleHomePage(request, response, parsedUrl);
    } else if (pathname === '/logs') {
      handleLogs(request, response, parsedUrl);
    } else if (pathname.match(/^\/file(\/.+)?$/)) {
      handleFile(request, response, parsedUrl);
    } else {
      response.writeHead(404); //Not Found
      response.end();
    }
  });
  server.listen(process.env.PORT || 8080);


  function handleHomePage(request, response, url) {
    response.writeHead(200);
    response.end("Homepage");
  }

  function handleLogs(request, response, url) {
    let from = url.query.from;
    let to = url.query.to;
    if (request.method === 'GET') {
      if (from && to) {
      log.read( from, to, (content) => {
        response.writeHead(200, {'Content-type': 'application/json'});
        response.end(content);
      })
      } else {
        log.readAll((content) => {
          response.writeHead(200, {'Content-type': 'application/json'});
          response.end(content);
        })
      }
    } else {
      response.writeHead(405); // Method not allowed
      response.end();
    }
      

  }

  function handleFile(request, response, url) {
    if (request.method === 'GET') {
      handleFileRead(request, response, url);
    } else if (request.method === 'POST') {
      handleFileWrite(request, response, url);
    } else {
      response.writeHead(405); // Method not allowed
      response.end();
    }
  }

  function handleFileRead(request, response, url) {
    let startIndex = url.pathname.indexOf('/', 1);
    let filename = url.pathname.slice(startIndex+1)
    fs.readFile(`./files/${filename}`, (err, content) => {
      flag = 'read';
      if (err) {
        response.writeHead(400, {'Content-type': 'text/html'});
        response.end(`No files with pathname ${filename} found`);
      } else {
        let message = `File with name '${filename}' read`;
        log.write(message);
      }
      response.writeHead(200, {'Content-type': 'text/html'});
      response.end(content);
    })
  }

  function handleFileWrite(request, response, url) {
    let filename = url.query.filename;
    let content = url.query.content;

    if (!validateParams(filename, content, response)) {
      return false;
    }

    flag = 'save';
    fs.writeFile(`./files/${filename}`, content, 'utf8', (err) => {
      if (err) {
        response.writeHead(500, {'Content-type': 'text/html'});
        response.end(`Failed to write file. Details: ${err.message}`);
      } else {
        let message = `New file with name '${filename}' saved`;
        log.write(message);
        response.writeHead(200, {'Content-type': 'text/html'});
        response.end(`File with filename ${filename} and content ${content} was created`);
      }
    });
  }

  function validateParams (filename, content, response) {
    let errorMessage;
    if (!filename || !filename.match(/^[\w\-. ]+$/)) {
      errorMessage = `Parameter 'filename' is missing or invalid`;
    } else if (content === undefined) {
      errorMessage = `Parameter 'content' not specified`;
    }

    if (errorMessage) {
      response.writeHead(400, {'Content-type': 'text/html'});
      response.end(errorMessage);
      return false;
    } else {
      return true;
    }
  }

}