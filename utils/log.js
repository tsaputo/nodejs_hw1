const fs = require('fs');
const path = './logs.json'

module.exports = {
  write: (message) => {
  fs.readFile(path, (err, content) => {
    if (err) { 
      let contentArr = {};
      contentArr.logs = [];
      contentArr.logs.push(new Event(message))
      fs.writeFile(path, JSON.stringify(contentArr), 'utf8', (err) => { if (err) throw err; }) 
    } else {
      let contentArr = JSON.parse(content);
      contentArr.logs.push(new Event(message));
      fs.writeFile(`./logs.json`, JSON.stringify(contentArr), 'utf8', (err) => { if (err) throw err;})
    }
    })    
  },

  readAll: (callback) => {
     fs.readFile(path, (err, content) => {
      callback(content);
     });
 },

 read: (from, to, callback) => {
  fs.readFile(path, (err, content) => {
    let file = JSON.parse(content);
    let filtredArr = file.logs.filter((elem) => elem.time >= from && elem.time <= to);
    callback(JSON.stringify({logs: filtredArr}));
  });
  }, 
}

class Event {
  constructor(message) {
    this.message = message;
    this.time = new Date().getTime();
  }
}
